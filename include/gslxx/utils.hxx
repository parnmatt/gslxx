#ifndef GSLXX__UTILS_HXX
#define GSLXX__UTILS_HXX

#include <cstddef>
#include <tuple>
#include <type_traits>
#include <utility>

namespace gslxx::utils {


	// index sequence

	template <std::size_t... Is>
	struct index_sequence {};

	namespace detail {
		template <std::size_t...>
		struct index_sequence_generator;

		template <std::size_t... Is>
		using index_sequence_generator_t = typename index_sequence_generator<Is...>::type;

		template <std::size_t B, std::size_t I, std::size_t... Is>
		struct index_sequence_generator<B, I, Is...> {
			using type = index_sequence_generator_t<B, I - 1, I - 1, Is...>;
		};

		template <std::size_t B, std::size_t... Is>
		struct index_sequence_generator<B, B, Is...> {
			using type = index_sequence<Is...>;
		};

	}  // namespace detail

	template <std::size_t E, std::size_t B = 0>
	using make_index_sequence = detail::index_sequence_generator_t<B, E>;


	// tuple from indices from tuple-like

	template <typename Tuple, std::size_t... Is>
	using tuple_from_index_sequence = std::tuple<std::tuple_element_t<Is, Tuple>...>;

	template <typename Tuple, std::size_t... Is>
	auto constexpr make_tuple_from_index_sequence(Tuple&& tuple, index_sequence<Is...>) noexcept {
		return tuple_from_index_sequence<Tuple, Is...>{std::get<Is>(std::forward<Tuple>(tuple))...};
	}

	template <std::size_t B, std::size_t E, typename Tuple>
	auto constexpr make_tuple_from_index_range(Tuple&& tuple) noexcept {
		return make_tuple_from_index_sequence(std::forward<Tuple>(tuple), make_index_sequence<E, B>{});
	}

	template <std::size_t E, typename Tuple>
	auto constexpr make_tuple_from_index_range(Tuple&& tuple) noexcept {
		return make_tuple_from_index_range<0, E>(std::forward<Tuple>(tuple));
	}

	template <typename Tuple, std::size_t E, std::size_t B = 0>
	using tuple_from_index_range = decltype(make_tuple_from_index_range<B, E>(std::declval<Tuple>()));


	// argument counter

	namespace detail {
		template <typename>
		struct arg_count_helper;

		template <typename T>
		using arg_count_helper_t = typename arg_count_helper<T>::type;

		template <typename R, typename T, bool Nx, typename... Args>
		struct arg_count_helper<R (T::*)(Args...) noexcept(Nx)> {
			using type = R(Args...);
		};

		template <typename R, typename T, bool Nx, typename... Args>
		struct arg_count_helper<R (T::*)(Args...) & noexcept(Nx)> {
			using type = R(Args...);
		};

		template <typename R, typename T, bool Nx, typename... Args>
		struct arg_count_helper<R (T::*)(Args...) const noexcept(Nx)> {
			using type = R(Args...);
		};

		template <typename R, typename T, bool Nx, typename... Args>
		struct arg_count_helper<R (T::*)(Args...) const& noexcept(Nx)> {
			using type = R(Args...);
		};

	}  // namespace detail

	template <typename>
	struct arg_count;

	template <typename F>
	auto constexpr inline arg_count_v = arg_count<F>::value;

	template <typename F>
	using arg_count_t = typename arg_count<F>::type;

	template <typename R, typename... Args>
	struct arg_count<R(Args...)> : std::integral_constant<std::size_t, sizeof...(Args)> {
		using type = std::tuple<Args...>;

		template <typename F>
		explicit constexpr arg_count(F&&) noexcept {}
	};

	template <typename R, typename... Args>
	struct arg_count<R (*)(Args...)> : arg_count<R(Args...)> {
		using arg_count<R(Args...)>::arg_count;
	};

	template <typename R, typename... Args>
	struct arg_count<R (&)(Args...)> : arg_count<R(Args...)> {
		using arg_count<R(Args...)>::arg_count;
	};

	template <typename R, typename... Args>
	arg_count(R(Args...))->arg_count<R(Args...)>;

	template <typename F>
	arg_count(F&&)->arg_count<detail::arg_count_helper_t<decltype(&F::operator())>>;


	// multivariate distribution functors from single callables

	template <std::size_t V, typename F>
	class distribution {
	private:
		using meta = arg_count<F>;
		using args_t = tuple_from_index_range<typename meta::type, V>;
		using params_t = tuple_from_index_range<typename meta::type, meta::value, V>;

	public:
		template <typename... Params, std::enable_if_t<std::is_constructible_v<params_t, Params...>, int> = 0>
		explicit constexpr distribution(F&& f, Params&&... params) noexcept
		    : m_f{std::forward<F>(f)}
		    , m_params{std::forward<Params>(params)...} {}

		template <typename... Params, std::enable_if_t<std::is_constructible_v<params_t, Params...>, int> = 0>
		explicit constexpr distribution(Params&&... params) noexcept
		    : m_params{std::forward<Params>(params)...} {}

		template <typename... Args, std::enable_if_t<std::is_constructible_v<args_t, Args...>, int> = 0>
		auto operator()(Args&&... args) const noexcept -> decltype(auto) {
			return std::apply(m_f, std::tuple_cat(std::forward_as_tuple(args...), m_params));
		}

	private:
		F m_f{};
		params_t m_params;
	};

	template <std::size_t V, typename F, typename... Params>
	auto constexpr make_distribution(F&& f, Params&&... params) noexcept {
		return distribution<V, F>{std::forward<F>(f), std::forward<Params>(params)...};
	}

	template <typename F>
	using univariate_distribution = distribution<1, F>;

	template <typename F, typename... Params>
	auto constexpr make_univariate_distribution(F&& f, Params&&... params) noexcept {
		return univariate_distribution<F>{std::forward<F>(f), std::forward<Params>(params)...};
	}

	template <typename F>
	using bivariate_distribution = distribution<2, F>;

	template <typename F, typename... Params>
	auto constexpr make_bivariate_distribution(F&& f, Params&&... params) noexcept {
		return bivariate_distribution<F>{std::forward<F>(f), std::forward<Params>(params)...};
	}

}  // namespace gslxx::utils

#endif  // ifndef GSLXX__UTILS_HXX
