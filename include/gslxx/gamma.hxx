#ifndef GSLXX__GAMMA_HXX
#define GSLXX__GAMMA_HXX

#include <complex>
#include <utility>

#include <gsl/gsl_cdf.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_sf_gamma.h>

#include "utils.hxx"

namespace gslxx {
	namespace pdf {

		using gamma_t = utils::univariate_distribution<decltype(&gsl_ran_gamma_pdf)>;

		template <typename... Params>
		auto constexpr gamma(Params&&... params) noexcept {
			return gamma_t{gsl_ran_gamma_pdf, std::forward<Params>(params)...};
		}

	}  // namespace pdf

	namespace cdf {

		using gamma_t = utils::univariate_distribution<decltype(&gsl_cdf_gamma_P)>;

		template <typename... Params>
		auto constexpr gamma(Params&&... params) noexcept {
			return gamma_t{gsl_cdf_gamma_P, std::forward<Params>(params)...};
		}


		using gamma_complement_t = utils::univariate_distribution<decltype(&gsl_cdf_gamma_Q)>;

		template <typename... Params>
		auto constexpr gamma_complement(Params&&... params) noexcept {
			return gamma_complement_t{gsl_cdf_gamma_Q, std::forward<Params>(params)...};
		}


		using inverse_gamma_t = utils::univariate_distribution<decltype(&gsl_cdf_gamma_Pinv)>;

		template <typename... Params>
		auto constexpr inverse_gamma(Params&&... params) noexcept {
			return inverse_gamma_t{gsl_cdf_gamma_Pinv, std::forward<Params>(params)...};
		}


		using inverse_gamma_complement_t = utils::univariate_distribution<decltype(&gsl_cdf_gamma_Qinv)>;

		template <typename... Params>
		auto constexpr inverse_gamma_complement(Params&&... params) noexcept {
			return inverse_gamma_complement_t{gsl_cdf_gamma_Qinv, std::forward<Params>(params)...};
		}

	}  // namespace cdf

	namespace sf {

		auto inline gamma(double x) noexcept { return gsl_sf_gamma(x); }
		auto inline ln_gamma(double x) noexcept { return gsl_sf_lngamma(x); }
		auto inline gamma(std::complex<double> z) noexcept {
			gsl_sf_result ln;
			gsl_sf_result arg;
			gsl_sf_lngamma_complex_e(std::real(z), std::imag(z), &ln, &arg);
			return std::pair{ln.val, arg.val};
		}

		auto inline sgn_gamma_ln_gamma(double x) noexcept {
			gsl_sf_result ln;
			double sgn;
			gsl_sf_lngamma_sgn_e(x, &ln, &sgn);
			return std::pair{sgn, ln.val};
		}

		auto inline regulated_gamma(double x) noexcept { return gsl_sf_gammastar(x); }
		auto inline reciprocal_gamma(double x) noexcept { return gsl_sf_gammainv(x); }
		auto inline upper_incomplete_gamma(double x, double low) noexcept { return gsl_sf_gamma_inc(x, low); }
		auto inline lower_incomplete_gamma(double x, double high) noexcept {
			return gamma(x) - upper_incomplete_gamma(x, high);
		}

		auto inline generalized_incomplete_gamma(double x, double low, double high) noexcept {
			return upper_incomplete_gamma(x, low) - upper_incomplete_gamma(x, high);
		}

		auto inline normalized_upper_incomplete_gamma(double x, double low) noexcept {
			return gsl_sf_gamma_inc_Q(x, low);
		}

		auto inline inverse_normalized_upper_incomplete_gamma(double x, double Q) noexcept {
			auto const Q_inv = cdf::inverse_gamma_complement(x, 1);
			return Q_inv(Q);
		}

		auto inline normalized_lower_incomplete_gamma(double x, double high) noexcept {
			return gsl_sf_gamma_inc_P(x, high);
		}

		auto inline inverse_normalized_lower_incomplete_gamma(double x, double P) noexcept {
			auto const P_inv = cdf::inverse_gamma(x, 1);
			return P_inv(P);
		}

		auto inline normalized_generalized_incomplete_gamma(double x, double low, double high) noexcept {
			return normalized_upper_incomplete_gamma(x, low) - normalized_upper_incomplete_gamma(x, high);
		}

	}  // namespace sf
}  // namespace gslxx

#endif  // ifndef GSLXX__GAMMA_HXX
